/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Angazho;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class AngazhoTableModel extends AbstractTableModel {

    List<Angazho> list;
    String[] cols = {"Nr.", "Emertimi", "Avokati", "Klienti", "Lenda", "Data", "LlojiPageses"};

    public AngazhoTableModel(List<Angazho> list) {
        this.list = list;
    }

    public AngazhoTableModel() {
    }

    public void addList(List<Angazho> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public Angazho getAngazho(int index) {
        return list.get(index);
    }

    public String getDateToString(Date d) {
        DateFormat da = new SimpleDateFormat("dd-MM-yyyy");
        return da.format(d);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Angazho a = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return a.getAngazhoID();
            case 1:
                return a.getEmertimi();
            case 2:
                return a.getAvokatiID();
            case 3:
                return a.getKlientiID();
            case 4:
                return a.getLendaID();
            case 5:
                return getDateToString(a.getData());
            case 6:
                return a.getLlojiPageses();
            default:
                return null;
        }
    }
}
