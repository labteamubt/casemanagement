/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.FaturaDalje;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class FaturaDaljeTableModel extends AbstractTableModel {

    List<FaturaDalje> list;
    String[] cols = {"Nr.", "Data","Totali","Angazhimi i Lendes"};

    public FaturaDaljeTableModel(List<FaturaDalje> list) {
        this.list = list;
    }

    public FaturaDaljeTableModel() {
    }

    public void addList(List<FaturaDalje> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public FaturaDalje getFaturaDalje(int index) {
        return list.get(index);
    }

    public String getDateToString(Date d) {
        DateFormat da = new SimpleDateFormat("dd-MM-yyyy");
        return da.format(d);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FaturaDalje fd = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return fd.getFdid();
            case 1:
                return getDateToString(fd.getData());
            case 2:
                return fd.getTotali();
            case 3:
                return fd.getAngazhoID();
            default:
                return null;
        }
    }
}
