/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Faza;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class FazaComboBoxModel extends AbstractListModel<Faza> implements ComboBoxModel<Faza> {

    private List<Faza> data;
    private Faza selectedItem;

    public FazaComboBoxModel(List<Faza> data) {
        this.data = data;
    }

    public FazaComboBoxModel() {
    }

    public void add(List<Faza> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Faza getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (Faza) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
