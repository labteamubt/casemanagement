/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Faza;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class FazaTableModel extends AbstractTableModel{
    List<Faza> list;
    String[] cols = {"Nr.", "Emri"};

    public FazaTableModel(List<Faza> list) {
        this.list = list;
    }

    public FazaTableModel() {
    }

    public void addList(List<Faza> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public Faza getFaza(int index) {
        return list.get(index);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Faza f = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return f.getFazaID();
            case 1:
                return f.getEmertimi();
            default:
                return null;
        }
    }
}
