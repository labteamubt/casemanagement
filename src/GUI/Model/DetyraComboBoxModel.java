/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Detyra;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class DetyraComboBoxModel extends AbstractListModel<Detyra> implements ComboBoxModel<Detyra> {

    private List<Detyra> data;
    private Detyra selectedItem;

    public DetyraComboBoxModel(List<Detyra> data) {
        this.data = data;
    }

    public DetyraComboBoxModel() {
    }

    public void add(List<Detyra> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Detyra getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (Detyra) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
