/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.LlojiProduktit;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class LlojiProduktitTableModel extends AbstractTableModel{

    List<LlojiProduktit> list;
    String[] cols = {"Nr.", "Emertimi"};

    public LlojiProduktitTableModel(List<LlojiProduktit> list) {
        this.list = list;
    }

    public LlojiProduktitTableModel() {
    }

    public void addList(List<LlojiProduktit> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public LlojiProduktit getLlojiProduktit(int index) {
        return list.get(index);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LlojiProduktit lp = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return lp.getLlojiProduktitID();
            case 1:
                return lp.getEmertimi();
            default:
                return null;
        }
    }
}
