/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.FaturaDalje;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class FaturaDaljeComboBoxModel extends AbstractListModel<FaturaDalje> implements ComboBoxModel<FaturaDalje> {

    private List<FaturaDalje> data;
    private FaturaDalje selectedItem;

    public FaturaDaljeComboBoxModel(List<FaturaDalje> data) {
        this.data = data;
    }

    public FaturaDaljeComboBoxModel() {
    }

    public void add(List<FaturaDalje> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public FaturaDalje getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (FaturaDalje) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
