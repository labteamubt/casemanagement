/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.FaturaHyrje;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeComboBoxModel extends AbstractListModel<FaturaHyrje> implements ComboBoxModel<FaturaHyrje> {

    private List<FaturaHyrje> data;
    private FaturaHyrje selectedItem;

    public FaturaHyrjeComboBoxModel(List<FaturaHyrje> data) {
        this.data = data;
    }

    public FaturaHyrjeComboBoxModel() {
    }

    public void add(List<FaturaHyrje> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public FaturaHyrje getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (FaturaHyrje) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
