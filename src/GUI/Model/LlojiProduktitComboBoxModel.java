/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.LlojiProduktit;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class LlojiProduktitComboBoxModel extends AbstractListModel<LlojiProduktit> implements ComboBoxModel<LlojiProduktit> {
    private List<LlojiProduktit> data;
    private LlojiProduktit selectedItem;

    public LlojiProduktitComboBoxModel(List<LlojiProduktit> data) {
        this.data = data;
    }

    public LlojiProduktitComboBoxModel() {
    }

    public void add(List<LlojiProduktit> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public LlojiProduktit getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (LlojiProduktit) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
