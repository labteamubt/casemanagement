/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.OrganizoDetyra;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class OrganizoDetyraTableModel extends AbstractTableModel{
     List<OrganizoDetyra> list;
    String[] cols = {"Nr.","Emri", "Detyra","Lenda", "Punonjes", "Prioriteti", "Pershkrimi", "Data"};

    public OrganizoDetyraTableModel(List<OrganizoDetyra> list) {
        this.list = list;
    }

    public OrganizoDetyraTableModel() {
    }

    public void addList(List<OrganizoDetyra> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public OrganizoDetyra getOrganizoDetyra(int index) {
        return list.get(index);
    }
    
    public String getDateToString(Date d){
        DateFormat da = new SimpleDateFormat("dd-MM-yyyy");
        return da.format(d);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OrganizoDetyra o= list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return o.getOrganizoID();
            case 1:
                return o.getEmri();
            case 2:
                return o.getDetyraID();
            case 3:
                return o.getLendaID();
            case 4:
                return o.getPunetoriID();
            case 5:
                return o.getPrioritetiID();
            case 6:
                return o.getPershkrimi();
            case 7:
                return getDateToString(o.getData());
            default:
                return null;
        }
    }
}
