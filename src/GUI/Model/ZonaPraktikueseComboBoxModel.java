/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.ZonaPraktikuese;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class ZonaPraktikueseComboBoxModel extends AbstractListModel<ZonaPraktikuese> implements ComboBoxModel<ZonaPraktikuese> {

    private List<ZonaPraktikuese> data;
    private ZonaPraktikuese selectedItem;

    public ZonaPraktikueseComboBoxModel(List<ZonaPraktikuese> data) {
        this.data = data;
    }

    public ZonaPraktikueseComboBoxModel() {
    }

    public void add(List<ZonaPraktikuese> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public ZonaPraktikuese getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (ZonaPraktikuese) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
