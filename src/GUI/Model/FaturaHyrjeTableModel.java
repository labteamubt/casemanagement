/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.FaturaHyrje;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeTableModel extends AbstractTableModel {

    List<FaturaHyrje> list;
    String[] cols = {"Nr.", "Furnitori", "Data", "Totali"};

    public FaturaHyrjeTableModel(List<FaturaHyrje> list) {
        this.list = list;
    }

    public FaturaHyrjeTableModel() {
    }

    public void addList(List<FaturaHyrje> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public FaturaHyrje getFaturaHyrje(int index) {
        return list.get(index);
    }

    public String getDateToString(Date d) {
        DateFormat da = new SimpleDateFormat("dd-MM-yyyy");
        return da.format(d);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FaturaHyrje fd = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return fd.getFhid();
            case 1:
                return fd.getFurnitoriID();
            case 2:
                return getDateToString(fd.getData());
            case 3:
                return fd.getTotali();
            default:
                return null;
        }
    }
}
