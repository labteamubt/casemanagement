/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Produkti;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class ProduktiTableModel extends AbstractTableModel {

    List<Produkti> list;
    String[] cols = {"Nr.", "Emertimi", "Lloji"};

    public ProduktiTableModel(List<Produkti> list) {
        this.list = list;
    }

    public ProduktiTableModel() {
    }

    public void addList(List<Produkti> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public Produkti getProdukti(int index) {
        return list.get(index);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Produkti p = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return p.getProduktiID();
            case 1:
                return p.getEmertimi();
            case 2:
                return p.getLlojiID();
            default:
                return null;
        }
    }
}
