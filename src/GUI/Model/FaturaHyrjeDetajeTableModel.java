/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.FaturaHyrjeDetaje;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeDetajeTableModel extends AbstractTableModel {

    List<FaturaHyrjeDetaje> list;
    String[] cols = {"Nr.", "Fatura Hyrje", "Produkti", "Shuma e blerjes"};

    public FaturaHyrjeDetajeTableModel(List<FaturaHyrjeDetaje> list) {
        this.list = list;
    }

    public FaturaHyrjeDetajeTableModel() {
    }

    public void addList(List<FaturaHyrjeDetaje> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public FaturaHyrjeDetaje getFaturaHyrjeDetaje(int index) {
        return list.get(index);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FaturaHyrjeDetaje fd = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return fd.getFhdid();
            case 1:
                return fd.getFhid();
            case 2:
                return fd.getProduktiID();
            case 3:
                return fd.getShumaBlerjes();
            default:
                return null;
        }
    }
}
