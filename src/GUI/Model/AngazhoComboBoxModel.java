/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;

import BLL.Angazho;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author RinorJahaj
 */
public class AngazhoComboBoxModel extends AbstractListModel<Angazho> implements ComboBoxModel<Angazho> {

    private List<Angazho> data;
    private Angazho selectedItem;

    public AngazhoComboBoxModel(List<Angazho> data) {
        this.data = data;
    }

    public AngazhoComboBoxModel() {
    }

    public void add(List<Angazho> data) {
        this.data = data;
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Angazho getElementAt(int index) {
        return data.get(index);
    }

    @Override
    public void setSelectedItem(Object anItem) {
        selectedItem = (Angazho) anItem;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
