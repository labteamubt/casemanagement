/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Model;


import BLL.ZonaPraktikuese;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author RinorJahaj
 */
public class ZonaPraktikueseTableModel extends AbstractTableModel {
     List<ZonaPraktikuese> list;
    String[] cols = {"Nr.", "Emertimi"};

    public ZonaPraktikueseTableModel(List<ZonaPraktikuese> list) {
        this.list = list;
    }

    public ZonaPraktikueseTableModel() {
    }

    public void addList(List<ZonaPraktikuese> list) {
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return cols.length;
    }

    @Override
    public String getColumnName(int col) {
        return cols[col];
    }

    public void remove(int row) {
        list.remove(row);
    }

    public ZonaPraktikuese getZonaPraktikuese(int index) {
        return list.get(index);
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ZonaPraktikuese zp = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return zp.getZonaID();
            case 1:
                return zp.getEmertimi();
            default:
                return null;
        }
    }
}
