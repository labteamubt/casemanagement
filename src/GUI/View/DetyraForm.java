/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.View;

import BLL.Detyra;
import BLL.Lenda;
import BLL.Prioriteti;
import BLL.Punonjes;
import DAL.CourtException;
import DAL.DetyraRepository;
import DAL.LendaRepository;
import DAL.PrioritetiRepository;
import DAL.PunonjesRepository;
import GUI.Model.DetyraTableModel;
import GUI.Model.LendaComboBoxModel;
import GUI.Model.PrioritetiComboBoxModel;
import GUI.Model.PunonjesComboBoxModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author RinorJahaj
 */
public class DetyraForm extends javax.swing.JInternalFrame {

    DetyraRepository dr = new DetyraRepository();
    DetyraTableModel dtm = new DetyraTableModel();
//    LendaRepository lr = new LendaRepository();
//    LendaComboBoxModel lcbm = new LendaComboBoxModel();
//    PunonjesRepository pr = new PunonjesRepository();
//    PunonjesComboBoxModel pcbm = new PunonjesComboBoxModel();
//    PrioritetiRepository prr = new PrioritetiRepository();
//    PrioritetiComboBoxModel pcbmm = new PrioritetiComboBoxModel();

    public DetyraForm() {
        initComponents();
        loadTable();
//        loadComboBox();
        tabelaSelectedIndexChange();
    }

    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Detyra d = dtm.getDetyra(selectedIndex);
                    idTxt.setText(d.getDetyraID() + "");
//                    lendaCb.setSelectedItem(d.getLendaID());
//                    lendaCb.repaint();
//                    punetoriCb.setSelectedItem(d.getPunetoriID());
//                    punetoriCb.repaint();
                    emriTxt.setText(d.getEmri());
//                    jDateChooser1.setDate(d.getData());
//                    prioritetiCb.setSelectedItem(d.getPrioritetiID());
//                    prioritetiCb.repaint();
//                    pershkrimiTxt.setText(d.getPershkrimi());

                }
            }
        });
    }

//    public void loadComboBox() {
//        try {
//            lcbm.add(lr.findAll());
//            pcbm.add(pr.findAll());
//            pcbmm.add(prr.findAll());
//            lendaCb.setModel(lcbm);
//            lendaCb.repaint();
//            punetoriCb.setModel(pcbm);
//            punetoriCb.repaint();
//            prioritetiCb.setModel(pcbmm);
//            prioritetiCb.repaint();
//        } catch (CourtException ex) {
//            Logger.getLogger(DetyraForm.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
    public void loadTable() {
        try {
            List<Detyra> lista = dr.findAll();
            dtm.addList(lista);
            table.setModel(dtm);
            dtm.fireTableDataChanged();
        } catch (CourtException ex) {
            Logger.getLogger(DetyraForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        idTxt = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        saveBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        emriTxt = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Detyra");

        jLabel1.setText("ID");

        jLabel6.setText("Emertimi");

        idTxt.setEditable(false);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(table);

        saveBtn.setText("Ruaj");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Anulo");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        deleteBtn.setText("Fshij");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel1)
                    .addComponent(saveBtn))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emriTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(55, 55, 55)
                        .addComponent(cancelBtn)
                        .addGap(51, 51, 51)
                        .addComponent(deleteBtn)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(emriTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveBtn)
                    .addComponent(cancelBtn)
                    .addComponent(deleteBtn))
                .addGap(33, 33, 33)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        int row = table.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja",
                    JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Detyra d = dtm.getDetyra(row);
                try {
                    dr.delete(d);
                } catch (CourtException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
                clear();
                loadTable();
            } else {
                clear();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteBtnActionPerformed
    public boolean verifikoString(String s) {
        char[] karakteret = s.toCharArray();
        for (char c : karakteret) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }
    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        try {
            int row = table.getSelectedRow();
            if (!emriTxt.getText().trim().equals("")) {
                if (row == -1) {
                    Detyra d = new Detyra();
                    if (!Character.isDigit(emriTxt.getText().charAt(0))) {
                        if (verifikoString(emriTxt.getText())) {
                            d.setEmri(emriTxt.getText());
                            dr.create(d);
                        } else {
                            JOptionPane.showMessageDialog(this, "Emri nuk duhet te permbaj numer apo simbol");
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Emri nuk duhet te filloj me numer!");
                    }

                } else {
//                    Detyra d = new Detyra();
//                    d.setEmri(emriTxt.getText());
//                    dr.edit(d);
                    JOptionPane.showMessageDialog(this, "Nuk mund te regjistrohet e dhena e njejt!");
                }
                clear();
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Nuk keni plotesuar te dhenat e nevojshme per regjistrim");
            }
        } catch (CourtException ex) {
            JOptionPane.showMessageDialog(this, "E dhena ekziston");
        }
    }//GEN-LAST:event_saveBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        if (!emriTxt.getText().equals("")) {
            clear();
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asnje te dhene per ta anuluar!");
        }

    }//GEN-LAST:event_cancelBtnActionPerformed
    public void clear() {
        table.clearSelection();
        idTxt.setText("");
//        lendaCb.setSelectedIndex(-1);
//        lendaCb.repaint();
//        punetoriCb.setSelectedIndex(-1);
//        punetoriCb.repaint();
        emriTxt.setText("");
//        prioritetiCb.setSelectedIndex(-1);
//        prioritetiCb.repaint();
//        pershkrimiTxt.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JTextField emriTxt;
    private javax.swing.JTextField idTxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
