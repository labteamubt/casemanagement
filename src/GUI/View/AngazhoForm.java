/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.View;

import BLL.Angazho;
import BLL.Avokati;
import BLL.Klienti;
import BLL.Lenda;
import BLL.LlojiPageses;
import DAL.AngazhoRepository;
import DAL.AvokatiRepository;
import DAL.CourtException;
import DAL.KlientiRepository;
import DAL.LendaRepository;
import DAL.LlojiPagesesRepository;
import GUI.Model.AngazhoTableModel;
import GUI.Model.AvokatiComboBoxModel;
import GUI.Model.KlientiComboBoxModel;
import GUI.Model.LendaComboBoxModel;
import GUI.Model.LlojiPagesesComboBoxModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDesktopPane;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author RinorJahaj
 */
public class AngazhoForm extends javax.swing.JInternalFrame {

    AngazhoRepository ar = new AngazhoRepository();
    AngazhoTableModel atm = new AngazhoTableModel();
    AvokatiRepository aar = new AvokatiRepository();
    AvokatiComboBoxModel acbm = new AvokatiComboBoxModel();
    KlientiRepository kr = new KlientiRepository();
    KlientiComboBoxModel kcbm = new KlientiComboBoxModel();
    LendaRepository lr = new LendaRepository();
    LendaComboBoxModel lcbm = new LendaComboBoxModel();
    LlojiPagesesRepository llr = new LlojiPagesesRepository();
    LlojiPagesesComboBoxModel llcbm = new LlojiPagesesComboBoxModel();

    public AngazhoForm() {
        initComponents();
        loadTable();
        loadComboBox();
        tabelaSelectedIndexChange();
    }

    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    Angazho a = atm.getAngazho(selectedIndex);
                    idTxt.setText(a.getAngazhoID() + "");
                    avokatiCb.setSelectedItem(a.getAvokatiID());
                    avokatiCb.repaint();
                    klientiCb.setSelectedItem(a.getKlientiID());
                    klientiCb.repaint();
                    lendaCb.setSelectedItem(a.getLendaID());
                    lendaCb.repaint();
                    llojiCb.setSelectedItem(a.getLlojiPageses());
                    llojiCb.repaint();
                    jDateChooser1.setDate(a.getData());
                    a.setEmertimi(emertimiTxt.getText());
                }
            }
        });
    }

    public void loadComboBox() {
        try {
            acbm.add(aar.findAll());
            kcbm.add(kr.findAll());
            lcbm.add(lr.findAll());
            llcbm.add(llr.findAll());
            avokatiCb.setModel(acbm);
            avokatiCb.repaint();
            klientiCb.setModel(kcbm);
            klientiCb.repaint();
            lendaCb.setModel(lcbm);
            lendaCb.repaint();
            llojiCb.setModel(llcbm);
            llojiCb.repaint();
        } catch (CourtException ex) {
            Logger.getLogger(LendaForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadTable() {
        try {
            List<Angazho> lista = ar.findAll();
            atm.addList(lista);
            table.setModel(atm);
            atm.fireTableDataChanged();
        } catch (CourtException ex) {
            Logger.getLogger(DetyraForm.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        idTxt = new javax.swing.JTextField();
        avokatiCb = new javax.swing.JComboBox();
        klientiCb = new javax.swing.JComboBox();
        lendaCb = new javax.swing.JComboBox();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        llojiCb = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        saveBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        kerkoTxt = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        emertimiTxt = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Angazho");

        jLabel1.setText("AngazhoID");

        jLabel2.setText("Avokati");

        jLabel3.setText("Klienti");

        jLabel4.setText("Lenda");

        jLabel5.setText("Data");

        jLabel6.setText("LlojiPageses");

        idTxt.setEditable(false);

        avokatiCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        klientiCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        lendaCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        llojiCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        saveBtn.setText("Ruaj");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        deleteBtn.setText("Fshij");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Anulo");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        jLabel8.setText("Kerko");

        jButton1.setText("Pagesa");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel9.setText("Emertimi");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(avokatiCb, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabel4)))
                            .addComponent(emertimiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cancelBtn)
                                .addGap(92, 92, 92)
                                .addComponent(deleteBtn))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(118, 118, 118)
                                    .addComponent(jButton1))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(lendaCb, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(68, 68, 68))
                                                .addComponent(klientiCb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jLabel5))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(183, 183, 183)
                                            .addComponent(jLabel6)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(llojiCb, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 119, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(saveBtn)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(kerkoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(306, 306, 306)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jLabel3)
                        .addComponent(jLabel5)
                        .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(klientiCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(avokatiCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lendaCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(llojiCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel9)
                    .addComponent(emertimiTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deleteBtn)
                    .addComponent(cancelBtn)
                    .addComponent(saveBtn))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(kerkoTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
     public boolean verifikoString(String s) {
        char[] karakteret = s.toCharArray();
        for (char c : karakteret) {
            if (!Character.isLetter(c)) {
                return false;
            }
        }
        return true;
    }
    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        try {
            int row = table.getSelectedRow();
            if (avokatiCb.getSelectedItem() != null && klientiCb.getSelectedItem() != null && lendaCb.getSelectedItem() != null
                    && llojiCb.getSelectedItem() != null) {
                if (row == -1) {
                    Angazho a = new Angazho();
                    if (avokatiCb.getSelectedItem() != null) {
                        a.setAvokatiID((Avokati) avokatiCb.getSelectedItem());
                        if (klientiCb.getSelectedItem() != null) {
                            a.setKlientiID((Klienti) klientiCb.getSelectedItem());
                            if (lendaCb.getSelectedItem() != null) {
                                a.setLendaID((Lenda) lendaCb.getSelectedItem());
                                if (llojiCb.getSelectedItem() != null) {
                                    a.setLlojiPageses((LlojiPageses) llojiCb.getSelectedItem());
                                    if (!Character.isDigit((emertimiTxt.getText().charAt(0)))) {
                                        if (verifikoString(emertimiTxt.getText())) {
                                            a.setEmertimi(emertimiTxt.getText());
                                            a.setData(jDateChooser1.getDate());
                                            ar.create(a);
                                        } else {
                                            JOptionPane.showMessageDialog(rootPane, closable);
                                        }
                                    } else {

                                    }
                                } else {
                                    JOptionPane.showMessageDialog(this, "Cakto llojin e pageses!");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Cakto lenden!");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Cakto klientin!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(this, "Cakto avokatin!");
                    }

                } else {
//                    Angazho a = new Angazho();
//                    a.setAvokatiID((Avokati) avokatiCb.getSelectedItem());
//                    a.setKlientiID((Klienti) klientiCb.getSelectedItem());
//                    a.setLendaID((Lenda) lendaCb.getSelectedItem());
//                    a.setLlojiPageses((LlojiPageses) llojiCb.getSelectedItem());
//                    a.setData(jDateChooser1.getDate());
//
//                    ar.edit(a);
                    JOptionPane.showMessageDialog(this, "Nuk mund te regjistrohet e dhena e njejt!");
                }
                clear();
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Nuk keni plotesuar te dhenat e nevojshme per regjistrim");
            }
        } catch (CourtException ex) {
            JOptionPane.showMessageDialog(this, "E dhena ekziston");
        }
    }//GEN-LAST:event_saveBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        if (!emertimiTxt.getText().trim().equals("") || avokatiCb.getSelectedItem() != null || klientiCb.getSelectedItem() != null || lendaCb.getSelectedItem() != null
                || llojiCb.getSelectedItem() != null && jDateChooser1.getDate().equals("")) {
            clear();
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni plotesuar asgje per ta anuluar!");
        }
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        int row = table.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja",
                    JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                Angazho a = atm.getAngazho(row);
                try {
                    ar.delete(a);
                } catch (CourtException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
                clear();
                loadTable();
            } else {
                clear();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        FaturaDaljeForm fd = new FaturaDaljeForm();
        JDesktopPane desktopPane = getDesktopPane();
        desktopPane.add(fd);
        fd.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    public void clear() {
        table.clearSelection();
        avokatiCb.setSelectedIndex(-1);
        avokatiCb.repaint();
        klientiCb.setSelectedIndex(-1);
        klientiCb.repaint();
        lendaCb.setSelectedIndex(-1);
        lendaCb.repaint();
        llojiCb.setSelectedIndex(-1);
        llojiCb.repaint();
        emertimiTxt.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox avokatiCb;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JTextField emertimiTxt;
    private javax.swing.JTextField idTxt;
    private javax.swing.JButton jButton1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField kerkoTxt;
    private javax.swing.JComboBox klientiCb;
    private javax.swing.JComboBox lendaCb;
    private javax.swing.JComboBox llojiCb;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
