/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.View;

import BLL.FaturaHyrje;
import BLL.FaturaHyrjeDetaje;
import BLL.Produkti;
import DAL.CourtException;
import DAL.FaturaHyrjeDetajeRepository;
import DAL.FaturaHyrjeRepository;
import DAL.ProduktiRepository;
import GUI.Model.FaturaHyrjeComboBoxModel;
import GUI.Model.FaturaHyrjeDetajeTableModel;
import GUI.Model.ProduktiComboBoxModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeDetajeForm extends javax.swing.JInternalFrame {

    FaturaHyrjeDetajeRepository fr = new FaturaHyrjeDetajeRepository();
    FaturaHyrjeDetajeTableModel ftm = new FaturaHyrjeDetajeTableModel();
    FaturaHyrjeRepository frr = new FaturaHyrjeRepository();
    FaturaHyrjeComboBoxModel fcbm = new FaturaHyrjeComboBoxModel();
    ProduktiRepository pr = new ProduktiRepository();
    ProduktiComboBoxModel pcbm = new ProduktiComboBoxModel();

    public FaturaHyrjeDetajeForm() {
        initComponents();
        tabelaSelectedIndexChange();
        loadComboBox();
        loadTable();
        
    }

    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    FaturaHyrjeDetaje fd = ftm.getFaturaHyrjeDetaje(selectedIndex);
                    idTxt.setText(fd.getFhdid() + "");
                    produktiCb.setSelectedItem(fd.getProduktiID());
                    produktiCb.repaint();
                    faturaCb.setSelectedItem(fd.getFhid());
                    faturaCb.repaint();
                    shumaTxt.setText(fd.getShumaBlerjes() + "");
                }
            }
        });
    }

    public void loadComboBox() {
        try {
            fcbm.add(frr.findAll());
            pcbm.add(pr.findAll());
            faturaCb.setModel(fcbm);
            faturaCb.repaint();
            produktiCb.setModel(pcbm);
            produktiCb.repaint();

        } catch (CourtException ex) {
            Logger.getLogger(FaturaHyrjeDetaje.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadTable() {
        try {
            List<FaturaHyrjeDetaje> lista = fr.findAll();
            ftm.addList(lista);
            table.setModel(ftm);
            ftm.fireTableDataChanged();
        } catch (CourtException ex) {
            Logger.getLogger(FaturaHyrjeDetaje.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        saveBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        faturaCb = new javax.swing.JComboBox();
        produktiCb = new javax.swing.JComboBox();
        idTxt = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        shumaTxt = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Fatura Hyrje Detaje");

        jLabel1.setText("ID:");

        jLabel2.setText("Fatura Hyrje:");

        jLabel3.setText("Produkti:");

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        saveBtn.setText("Ruaj");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Anulo");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        deleteBtn.setText("Fshij");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        faturaCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        produktiCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        idTxt.setEditable(false);

        jLabel5.setText("Shuma Blerjes");

        shumaTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shumaTxtActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(12, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(faturaCb, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(110, 110, 110)
                                        .addComponent(jLabel3)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(produktiCb, 0, 142, Short.MAX_VALUE)
                                    .addComponent(shumaTxt)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(99, 99, 99)
                                .addComponent(saveBtn)
                                .addGap(44, 44, 44)
                                .addComponent(cancelBtn)
                                .addGap(52, 52, 52)
                                .addComponent(deleteBtn)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(produktiCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(faturaCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(shumaTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveBtn)
                    .addComponent(cancelBtn)
                    .addComponent(deleteBtn))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
 private boolean verifikoNr(double nr) {
        if (nr != 0 || nr > 0) {
            return true;
        }
        return false;

    }

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        try {
//            FaturaHyrje ff = new FaturaHyrje();
//            FaturaHyrjeDetaje fd = new FaturaHyrjeDetaje();
//            int totali = ff.getTotali();
//            int sasia = fd.getSasia();
//            int shumaBlerjes = totali + sasia;
            
            int row = table.getSelectedRow();
            if (produktiCb.getSelectedItem() != null && faturaCb.getSelectedItem() != null || !shumaTxt.getText().trim().equals("")) {
                if (row == -1) {
                    FaturaHyrjeDetaje fd = new FaturaHyrjeDetaje();
                    if (produktiCb.getSelectedItem() != null) {
                        fd.setProduktiID((Produkti) produktiCb.getSelectedItem());
                        if (Character.isDigit(shumaTxt.getText().charAt(0))) {
                            if (verifikoNr((Double.parseDouble(shumaTxt.getText())))) {
//                                if (verifikoNumrin((Double.parseDouble(totaliTxt.getText())))) {
                                fd.setShumaBlerjes(Double.parseDouble(shumaTxt.getText()));
                                if (faturaCb.getSelectedItem() != null) {
                                    fd.setFhid((FaturaHyrje) faturaCb.getSelectedItem());
                                    fr.create(fd);
                                } else {
                                    JOptionPane.showMessageDialog(this, "Cakto Faturen!");
                                }
//                                } else {
//                                    JOptionPane.showMessageDialog(this, "Numrddi i dhene jo validi!");
//                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Numri i dhene jo validi!");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Numri i dhene jo valid!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(this, "Cakto Produktin!");
                    }

                } else {

                    JOptionPane.showMessageDialog(this, "Nuk mund te regjistrohet e dhena e njejt!");
                }
                clear();
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Nuk keni plotesuar te dhenat e nevojshme per regjistrim");
            }
        } catch (CourtException ex) {
            JOptionPane.showMessageDialog(this, "E dhena ekziston");
        }
    }//GEN-LAST:event_saveBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        int row = table.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja",
                    JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                FaturaHyrjeDetaje fd = ftm.getFaturaHyrjeDetaje(row);
                try {
                    fr.delete(fd);
                } catch (CourtException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
                clear();
                loadTable();
            } else {
                clear();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        if (produktiCb.getSelectedItem() != null
                || !shumaTxt.getText().trim().equals("") || faturaCb.getSelectedItem() != null) {
            clear();
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni plotesuar asgje per ta anuluar!");

        }
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void shumaTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shumaTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_shumaTxtActionPerformed
    public void clear() {
        idTxt.setText("");
        faturaCb.setSelectedIndex(-1);
        faturaCb.repaint();
        produktiCb.setSelectedIndex(-1);
        produktiCb.repaint();
        shumaTxt.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JComboBox faturaCb;
    private javax.swing.JTextField idTxt;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox produktiCb;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTextField shumaTxt;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
