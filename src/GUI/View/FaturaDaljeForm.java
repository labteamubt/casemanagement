/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.View;

import BLL.Angazho;
import BLL.FaturaDalje;
import DAL.AngazhoRepository;
import DAL.CourtException;
import DAL.FaturaDaljeRepository;
import GUI.Model.AngazhoComboBoxModel;
import GUI.Model.FaturaDaljeTableModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author RinorJahaj
 */
public class FaturaDaljeForm extends javax.swing.JInternalFrame {

    FaturaDaljeRepository fdr = new FaturaDaljeRepository();
    FaturaDaljeTableModel fdtm = new FaturaDaljeTableModel();
    AngazhoRepository ar = new AngazhoRepository();
    AngazhoComboBoxModel acbm = new AngazhoComboBoxModel();

    public FaturaDaljeForm() {
        initComponents();
        loadTable();
        loadComboBox();
        tabelaSelectedIndexChange();
    }

    private void tabelaSelectedIndexChange() {
        final ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent Ise) {
                if (Ise.getValueIsAdjusting()) {
                    return;
                }
                ListSelectionModel rowSM = (ListSelectionModel) Ise.getSource();
                int selectedIndex = rowSM.getAnchorSelectionIndex();
                if (selectedIndex > -1) {
                    FaturaDalje fd = fdtm.getFaturaDalje(selectedIndex);
                    idTxt.setText(fd.getFdid() + "");
                    angazhoCb.setSelectedItem(fd.getAngazhoID());
                    angazhoCb.repaint();
                    jDateChooser1.setDate(fd.getData());
                    totaliTxt.setText(fd.getTotali() + "");
                    // totaliTxt.setText((Double.parseDouble(totaliTxt.getText())));
                }
            }
        });
    }

    public void loadComboBox() {
        try {
            acbm.add(ar.findAll());
            angazhoCb.setModel(acbm);
            angazhoCb.repaint();

        } catch (CourtException ex) {
            Logger.getLogger(FaturaDalje.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadTable() {
        try {
            List<FaturaDalje> lista = fdr.findAll();
            fdtm.addList(lista);
            table.setModel(fdtm);
            fdtm.fireTableDataChanged();
        } catch (CourtException ex) {
            Logger.getLogger(FaturaDalje.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        angazhoCb = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        idTxt = new javax.swing.JTextField();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        totaliTxt = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        saveBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Fatura Dalje");

        jLabel1.setText("ID:");

        jLabel2.setText("Data:");

        jLabel3.setText("Totali:");

        angazhoCb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel4.setText("Angazhimi:");

        idTxt.setEditable(false);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(table);

        saveBtn.setText("Ruaj");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Anulo");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        deleteBtn.setText("Fshij");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(saveBtn)
                                .addGap(52, 52, 52)
                                .addComponent(cancelBtn)
                                .addGap(54, 54, 54)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(deleteBtn, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(angazhoCb, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(totaliTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(angazhoCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(idTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(jLabel3)
                        .addComponent(totaliTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveBtn)
                    .addComponent(cancelBtn)
                    .addComponent(deleteBtn))
                .addGap(32, 32, 32)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private boolean verifikoNr(double nr) {
        if (nr != 0 || nr > 0) {
            return true;
        }
        return false;

    }

    private boolean verifikoNumrin(double nr) {
        String n = new Double(nr).toString();
        char[] karaktere = n.toCharArray();
        for (char c : karaktere) {
            if (!Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }
    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        try {
            int row = table.getSelectedRow();
            if (angazhoCb.getSelectedItem() != null && !totaliTxt.getText().trim().equals("")) {
                if (row == -1) {
                    FaturaDalje fd = new FaturaDalje();
                    if (angazhoCb.getSelectedItem() != null) {
                        fd.setAngazhoID((Angazho) angazhoCb.getSelectedItem());
                        if (Character.isDigit(totaliTxt.getText().charAt(0))) {
                            if (verifikoNr((Double.parseDouble(totaliTxt.getText())))) {
                                if (verifikoNumrin((Double.parseDouble(totaliTxt.getText())))) {
                                    fd.setTotali(Double.parseDouble(totaliTxt.getText()));
                                    fd.setData(jDateChooser1.getDate());
                                    fdr.create(fd);
                                } else {
                                    JOptionPane.showMessageDialog(this, "Numrddi i dhene jo validi!");
                                }
                            } else {
                                JOptionPane.showMessageDialog(this, "Numri i dhene jo validi!");
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Numri i dhene jo valid!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(this, "Cakto Angazhimin!");
                    }

                } else {

                    JOptionPane.showMessageDialog(this, "Nuk mund te regjistrohet e dhena e njejt!");
                }
                clear();
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Nuk keni plotesuar te dhenat e nevojshme per regjistrim");
            }
        } catch (CourtException ex) {
            JOptionPane.showMessageDialog(this, "E dhena ekziston");
        }
    }//GEN-LAST:event_saveBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        if (angazhoCb.getSelectedItem() != null
                || !totaliTxt.getText().trim().equals("")) {
            clear();
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni plotesuar asgje per ta anuluar!");

        }
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        int row = table.getSelectedRow();
        if (row != -1) {
            Object[] ob = {"Po", "Jo"};
            int i = JOptionPane.showOptionDialog(this, "A dëshironi ta fshini ?", "Fshirja",
                    JOptionPane.OK_OPTION, JOptionPane.QUESTION_MESSAGE, null, ob, ob[1]);
            if (i == 0) {
                FaturaDalje fd = fdtm.getFaturaDalje(row);
                try {
                    fdr.delete(fd);
                } catch (CourtException ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage());
                }
                clear();
                loadTable();
            } else {
                clear();
            }
        } else {
            JOptionPane.showMessageDialog(this, "Nuk keni selektuar asgje per te fshire!");
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    public void clear() {
        idTxt.setText("");
        totaliTxt.setText("");
        angazhoCb.setSelectedIndex(-1);
        angazhoCb.repaint();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox angazhoCb;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JTextField idTxt;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTable table;
    private javax.swing.JTextField totaliTxt;
    // End of variables declaration//GEN-END:variables
}
