/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "Detyra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detyra.findAll", query = "SELECT d FROM Detyra d")
    , @NamedQuery(name = "Detyra.findByDetyraID", query = "SELECT d FROM Detyra d WHERE d.detyraID = :detyraID")
    , @NamedQuery(name = "Detyra.findByEmri", query = "SELECT d FROM Detyra d WHERE d.emri = :emri")})
public class Detyra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DetyraID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer detyraID;
    @Column(name = "Emri")
    private String emri;

    public Detyra() {
    }

    public Detyra(Integer detyraID) {
        this.detyraID = detyraID;
    }

    public Integer getDetyraID() {
        return detyraID;
    }

    public void setDetyraID(Integer detyraID) {
        this.detyraID = detyraID;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detyraID != null ? detyraID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detyra)) {
            return false;
        }
        Detyra other = (Detyra) object;
        if ((this.detyraID == null && other.detyraID != null) || (this.detyraID != null && !this.detyraID.equals(other.detyraID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return detyraID + " - " + emri;
    }
    
}
