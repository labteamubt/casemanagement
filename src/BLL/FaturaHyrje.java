/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "FaturaHyrje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FaturaHyrje.findAll", query = "SELECT f FROM FaturaHyrje f")
    , @NamedQuery(name = "FaturaHyrje.findByFhid", query = "SELECT f FROM FaturaHyrje f WHERE f.fhid = :fhid")
    , @NamedQuery(name = "FaturaHyrje.findByData", query = "SELECT f FROM FaturaHyrje f WHERE f.data = :data")
    , @NamedQuery(name = "FaturaHyrje.findByTotali", query = "SELECT f FROM FaturaHyrje f WHERE f.totali = :totali")})
public class FaturaHyrje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FHID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer fhid;
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Column(name = "Totali")
    private Integer totali;
    @JoinColumn(name = "FurnitoriID", referencedColumnName = "FurnitoriID")
    @ManyToOne
    private Furnitori furnitoriID;
    @OneToMany(mappedBy = "fhid")
    private Collection<FaturaHyrjeDetaje> faturaHyrjeDetajeCollection;

    public FaturaHyrje() {
    }

    public FaturaHyrje(Integer fhid) {
        this.fhid = fhid;
    }

    public Integer getFhid() {
        return fhid;
    }

    public void setFhid(Integer fhid) {
        this.fhid = fhid;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getTotali() {
        return totali;
    }

    public void setTotali(Integer totali) {
        this.totali = totali;
    }

    public Furnitori getFurnitoriID() {
        return furnitoriID;
    }

    public void setFurnitoriID(Furnitori furnitoriID) {
        this.furnitoriID = furnitoriID;
    }

    @XmlTransient
    public Collection<FaturaHyrjeDetaje> getFaturaHyrjeDetajeCollection() {
        return faturaHyrjeDetajeCollection;
    }

    public void setFaturaHyrjeDetajeCollection(Collection<FaturaHyrjeDetaje> faturaHyrjeDetajeCollection) {
        this.faturaHyrjeDetajeCollection = faturaHyrjeDetajeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhid != null ? fhid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FaturaHyrje)) {
            return false;
        }
        FaturaHyrje other = (FaturaHyrje) object;
        if ((this.fhid == null && other.fhid != null) || (this.fhid != null && !this.fhid.equals(other.fhid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return fhid + "";
    }

}
