/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "Produkti")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produkti.findAll", query = "SELECT p FROM Produkti p")
    , @NamedQuery(name = "Produkti.findByProduktiID", query = "SELECT p FROM Produkti p WHERE p.produktiID = :produktiID")
    , @NamedQuery(name = "Produkti.findByEmertimi", query = "SELECT p FROM Produkti p WHERE p.emertimi = :emertimi")})
public class Produkti implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ProduktiID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer produktiID;
    @Column(name = "Emertimi")
    private String emertimi;
    @JoinColumn(name = "LlojiID", referencedColumnName = "LlojiProduktitID")
    @ManyToOne
    private LlojiProduktit llojiID;
    @OneToMany(mappedBy = "produktiID")
    private Collection<FaturaHyrjeDetaje> faturaHyrjeDetajeCollection;

    public Produkti() {
    }

    public Produkti(Integer produktiID) {
        this.produktiID = produktiID;
    }

    public Integer getProduktiID() {
        return produktiID;
    }

    public void setProduktiID(Integer produktiID) {
        this.produktiID = produktiID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    public LlojiProduktit getLlojiID() {
        return llojiID;
    }

    public void setLlojiID(LlojiProduktit llojiID) {
        this.llojiID = llojiID;
    }

    @XmlTransient
    public Collection<FaturaHyrjeDetaje> getFaturaHyrjeDetajeCollection() {
        return faturaHyrjeDetajeCollection;
    }

    public void setFaturaHyrjeDetajeCollection(Collection<FaturaHyrjeDetaje> faturaHyrjeDetajeCollection) {
        this.faturaHyrjeDetajeCollection = faturaHyrjeDetajeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produktiID != null ? produktiID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produkti)) {
            return false;
        }
        Produkti other = (Produkti) object;
        if ((this.produktiID == null && other.produktiID != null) || (this.produktiID != null && !this.produktiID.equals(other.produktiID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return emertimi;
    }

}
