/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "LlojiProduktit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LlojiProduktit.findAll", query = "SELECT l FROM LlojiProduktit l")
    , @NamedQuery(name = "LlojiProduktit.findByLlojiProduktitID", query = "SELECT l FROM LlojiProduktit l WHERE l.llojiProduktitID = :llojiProduktitID")
    , @NamedQuery(name = "LlojiProduktit.findByEmertimi", query = "SELECT l FROM LlojiProduktit l WHERE l.emertimi = :emertimi")})
public class LlojiProduktit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "LlojiProduktitID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer llojiProduktitID;
    @Column(name = "Emertimi")
    private String emertimi;
    @OneToMany(mappedBy = "llojiID")
    private Collection<Produkti> produktiCollection;

    public LlojiProduktit() {
    }

    public LlojiProduktit(Integer llojiProduktitID) {
        this.llojiProduktitID = llojiProduktitID;
    }

    public Integer getLlojiProduktitID() {
        return llojiProduktitID;
    }

    public void setLlojiProduktitID(Integer llojiProduktitID) {
        this.llojiProduktitID = llojiProduktitID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    @XmlTransient
    public Collection<Produkti> getProduktiCollection() {
        return produktiCollection;
    }

    public void setProduktiCollection(Collection<Produkti> produktiCollection) {
        this.produktiCollection = produktiCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (llojiProduktitID != null ? llojiProduktitID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LlojiProduktit)) {
            return false;
        }
        LlojiProduktit other = (LlojiProduktit) object;
        if ((this.llojiProduktitID == null && other.llojiProduktitID != null) || (this.llojiProduktitID != null && !this.llojiProduktitID.equals(other.llojiProduktitID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return emertimi;
    }

}
