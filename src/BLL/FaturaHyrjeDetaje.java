/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "FaturaHyrjeDetaje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FaturaHyrjeDetaje.findAll", query = "SELECT f FROM FaturaHyrjeDetaje f")
    , @NamedQuery(name = "FaturaHyrjeDetaje.findByFhdid", query = "SELECT f FROM FaturaHyrjeDetaje f WHERE f.fhdid = :fhdid")
    , @NamedQuery(name = "FaturaHyrjeDetaje.findByShumaBlerjes", query = "SELECT f FROM FaturaHyrjeDetaje f WHERE f.shumaBlerjes = :shumaBlerjes")})
public class FaturaHyrjeDetaje implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FHDID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)

    private Integer fhdid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ShumaBlerjes")
    private Double shumaBlerjes;
    @JoinColumn(name = "FHID", referencedColumnName = "FHID")
    @ManyToOne
    private FaturaHyrje fhid;
    @JoinColumn(name = "ProduktiID", referencedColumnName = "ProduktiID")
    @ManyToOne
    private Produkti produktiID;

    public FaturaHyrjeDetaje() {
    }

    public FaturaHyrjeDetaje(Integer fhdid) {
        this.fhdid = fhdid;
    }

    public Integer getFhdid() {
        return fhdid;
    }

    public void setFhdid(Integer fhdid) {
        this.fhdid = fhdid;
    }

    public Double getShumaBlerjes() {
        return shumaBlerjes;
    }

    public void setShumaBlerjes(Double shumaBlerjes) {
        this.shumaBlerjes = shumaBlerjes;
    }

    public FaturaHyrje getFhid() {
        return fhid;
    }

    public void setFhid(FaturaHyrje fhid) {
        this.fhid = fhid;
    }

    public Produkti getProduktiID() {
        return produktiID;
    }

    public void setProduktiID(Produkti produktiID) {
        this.produktiID = produktiID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhdid != null ? fhdid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FaturaHyrjeDetaje)) {
            return false;
        }
        FaturaHyrjeDetaje other = (FaturaHyrjeDetaje) object;
        if ((this.fhdid == null && other.fhdid != null) || (this.fhdid != null && !this.fhdid.equals(other.fhdid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.FaturaHyrjeDetaje[ fhdid=" + fhdid + " ]";
    }

}
