/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "FaturaDalje")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FaturaDalje.findAll", query = "SELECT f FROM FaturaDalje f")
    , @NamedQuery(name = "FaturaDalje.findByFdid", query = "SELECT f FROM FaturaDalje f WHERE f.fdid = :fdid")
    , @NamedQuery(name = "FaturaDalje.findByData", query = "SELECT f FROM FaturaDalje f WHERE f.data = :data")
    , @NamedQuery(name = "FaturaDalje.findByTotali", query = "SELECT f FROM FaturaDalje f WHERE f.totali = :totali")})
public class FaturaDalje implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FDID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer fdid;
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Totali")
    private Double totali;
    @JoinColumn(name = "AngazhoID", referencedColumnName = "AngazhoID")
    @ManyToOne
    private Angazho angazhoID;

    public FaturaDalje() {
    }

    public FaturaDalje(Integer fdid) {
        this.fdid = fdid;
    }

    public Integer getFdid() {
        return fdid;
    }

    public void setFdid(Integer fdid) {
        this.fdid = fdid;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Double getTotali() {
        return totali;
    }

    public void setTotali(Double totali) {
        this.totali = totali;
    }

    public Angazho getAngazhoID() {
        return angazhoID;
    }

    public void setAngazhoID(Angazho angazhoID) {
        this.angazhoID = angazhoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fdid != null ? fdid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FaturaDalje)) {
            return false;
        }
        FaturaDalje other = (FaturaDalje) object;
        if ((this.fdid == null && other.fdid != null) || (this.fdid != null && !this.fdid.equals(other.fdid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return fdid + "";
    }

}
