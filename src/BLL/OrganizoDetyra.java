/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "OrganizoDetyra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrganizoDetyra.findAll", query = "SELECT o FROM OrganizoDetyra o")
    , @NamedQuery(name = "OrganizoDetyra.findByOrganizoID", query = "SELECT o FROM OrganizoDetyra o WHERE o.organizoID = :organizoID")
    , @NamedQuery(name = "OrganizoDetyra.findByEmri", query = "SELECT o FROM OrganizoDetyra o WHERE o.emri = :emri")
    , @NamedQuery(name = "OrganizoDetyra.findByPershkrimi", query = "SELECT o FROM OrganizoDetyra o WHERE o.pershkrimi = :pershkrimi")
    , @NamedQuery(name = "OrganizoDetyra.findByData", query = "SELECT o FROM OrganizoDetyra o WHERE o.data = :data")})
public class OrganizoDetyra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "OrganizoID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer organizoID;
    @Column(name = "Emri")
    private String emri;
    @Column(name = "Pershkrimi")
    private String pershkrimi;
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @JoinColumn(name = "DetyraID", referencedColumnName = "DetyraID")
    @ManyToOne
    private Detyra detyraID;
    @JoinColumn(name = "LendaID", referencedColumnName = "LendaID")
    @ManyToOne
    private Lenda lendaID;
    @JoinColumn(name = "PrioritetiID", referencedColumnName = "PrioritetiID")
    @ManyToOne
    private Prioriteti prioritetiID;
    @JoinColumn(name = "PunetoriID", referencedColumnName = "PunonjesID")
    @ManyToOne
    private Punonjes punetoriID;

    public OrganizoDetyra() {
    }

    public OrganizoDetyra(Integer organizoID) {
        this.organizoID = organizoID;
    }

    public Integer getOrganizoID() {
        return organizoID;
    }

    public void setOrganizoID(Integer organizoID) {
        this.organizoID = organizoID;
    }

    public String getEmri() {
        return emri;
    }

    public void setEmri(String emri) {
        this.emri = emri;
    }

    public String getPershkrimi() {
        return pershkrimi;
    }

    public void setPershkrimi(String pershkrimi) {
        this.pershkrimi = pershkrimi;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Detyra getDetyraID() {
        return detyraID;
    }

    public void setDetyraID(Detyra detyraID) {
        this.detyraID = detyraID;
    }

    public Lenda getLendaID() {
        return lendaID;
    }

    public void setLendaID(Lenda lendaID) {
        this.lendaID = lendaID;
    }

    public Prioriteti getPrioritetiID() {
        return prioritetiID;
    }

    public void setPrioritetiID(Prioriteti prioritetiID) {
        this.prioritetiID = prioritetiID;
    }

    public Punonjes getPunetoriID() {
        return punetoriID;
    }

    public void setPunetoriID(Punonjes punetoriID) {
        this.punetoriID = punetoriID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizoID != null ? organizoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganizoDetyra)) {
            return false;
        }
        OrganizoDetyra other = (OrganizoDetyra) object;
        if ((this.organizoID == null && other.organizoID != null) || (this.organizoID != null && !this.organizoID.equals(other.organizoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BLL.OrganizoDetyra[ organizoID=" + organizoID + " ]";
    }

}
