/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "Angazho")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Angazho.findAll", query = "SELECT a FROM Angazho a")
    , @NamedQuery(name = "Angazho.findByAngazhoID", query = "SELECT a FROM Angazho a WHERE a.angazhoID = :angazhoID")
    , @NamedQuery(name = "Angazho.findByData", query = "SELECT a FROM Angazho a WHERE a.data = :data")
    , @NamedQuery(name = "Angazho.findByEmertimi", query = "SELECT a FROM Angazho a WHERE a.emertimi = :emertimi")})
public class Angazho implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AngazhoID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer angazhoID;
    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Column(name = "Emertimi")
    private String emertimi;
    @JoinColumn(name = "AvokatiID", referencedColumnName = "AvokatiID")
    @ManyToOne
    private Avokati avokatiID;
    @JoinColumn(name = "KlientiID", referencedColumnName = "KlientiID")
    @ManyToOne
    private Klienti klientiID;
    @JoinColumn(name = "LendaID", referencedColumnName = "LendaID")
    @ManyToOne
    private Lenda lendaID;
    @JoinColumn(name = "LlojiPageses", referencedColumnName = "QmimiID")
    @ManyToOne
    private LlojiPageses llojiPageses;
    @OneToMany(mappedBy = "angazhoID")
    private Collection<FaturaDalje> faturaDaljeCollection;

    public Angazho() {
    }

    public Angazho(Integer angazhoID) {
        this.angazhoID = angazhoID;
    }

    public Integer getAngazhoID() {
        return angazhoID;
    }

    public void setAngazhoID(Integer angazhoID) {
        this.angazhoID = angazhoID;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    public Avokati getAvokatiID() {
        return avokatiID;
    }

    public void setAvokatiID(Avokati avokatiID) {
        this.avokatiID = avokatiID;
    }

    public Klienti getKlientiID() {
        return klientiID;
    }

    public void setKlientiID(Klienti klientiID) {
        this.klientiID = klientiID;
    }

    public Lenda getLendaID() {
        return lendaID;
    }

    public void setLendaID(Lenda lendaID) {
        this.lendaID = lendaID;
    }

    public LlojiPageses getLlojiPageses() {
        return llojiPageses;
    }

    public void setLlojiPageses(LlojiPageses llojiPageses) {
        this.llojiPageses = llojiPageses;
    }

    @XmlTransient
    public Collection<FaturaDalje> getFaturaDaljeCollection() {
        return faturaDaljeCollection;
    }

    public void setFaturaDaljeCollection(Collection<FaturaDalje> faturaDaljeCollection) {
        this.faturaDaljeCollection = faturaDaljeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (angazhoID != null ? angazhoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Angazho)) {
            return false;
        }
        Angazho other = (Angazho) object;
        if ((this.angazhoID == null && other.angazhoID != null) || (this.angazhoID != null && !this.angazhoID.equals(other.angazhoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return angazhoID + " - " + emertimi;
    }

}
