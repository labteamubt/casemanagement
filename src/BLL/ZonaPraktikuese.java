/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "ZonaPraktikuese")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ZonaPraktikuese.findAll", query = "SELECT z FROM ZonaPraktikuese z")
    , @NamedQuery(name = "ZonaPraktikuese.findByZonaID", query = "SELECT z FROM ZonaPraktikuese z WHERE z.zonaID = :zonaID")
    , @NamedQuery(name = "ZonaPraktikuese.findByEmertimi", query = "SELECT z FROM ZonaPraktikuese z WHERE z.emertimi = :emertimi")})
public class ZonaPraktikuese implements Serializable {

    @OneToMany(mappedBy = "zonaPraktikueseID")
    private Collection<Lenda> lendaCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ZonaID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer zonaID;
    @Column(name = "Emertimi")
    private String emertimi;

    public ZonaPraktikuese() {
    }

    public ZonaPraktikuese(Integer zonaID) {
        this.zonaID = zonaID;
    }

    public Integer getZonaID() {
        return zonaID;
    }

    public void setZonaID(Integer zonaID) {
        this.zonaID = zonaID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (zonaID != null ? zonaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ZonaPraktikuese)) {
            return false;
        }
        ZonaPraktikuese other = (ZonaPraktikuese) object;
        if ((this.zonaID == null && other.zonaID != null) || (this.zonaID != null && !this.zonaID.equals(other.zonaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return emertimi;
    }

    @XmlTransient
    public Collection<Lenda> getLendaCollection() {
        return lendaCollection;
    }

    public void setLendaCollection(Collection<Lenda> lendaCollection) {
        this.lendaCollection = lendaCollection;
    }

}
