/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author RinorJahaj
 */
@Entity
@Table(name = "Faza")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Faza.findAll", query = "SELECT f FROM Faza f")
    , @NamedQuery(name = "Faza.findByFazaID", query = "SELECT f FROM Faza f WHERE f.fazaID = :fazaID")
    , @NamedQuery(name = "Faza.findByEmertimi", query = "SELECT f FROM Faza f WHERE f.emertimi = :emertimi")})
public class Faza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "FazaID")
    @GeneratedValue(generator = "InvSeq")
    @SequenceGenerator(name = "InvSeq", sequenceName = "INV_SEQ", allocationSize = 1)
    private Integer fazaID;
    @Column(name = "Emertimi")
    private String emertimi;
    @OneToMany(mappedBy = "faza")
    private Collection<Lenda> lendaCollection;

    public Faza() {
    }

    public Faza(Integer fazaID) {
        this.fazaID = fazaID;
    }

    public Integer getFazaID() {
        return fazaID;
    }

    public void setFazaID(Integer fazaID) {
        this.fazaID = fazaID;
    }

    public String getEmertimi() {
        return emertimi;
    }

    public void setEmertimi(String emertimi) {
        this.emertimi = emertimi;
    }

    @XmlTransient
    public Collection<Lenda> getLendaCollection() {
        return lendaCollection;
    }

    public void setLendaCollection(Collection<Lenda> lendaCollection) {
        this.lendaCollection = lendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fazaID != null ? fazaID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Faza)) {
            return false;
        }
        Faza other = (Faza) object;
        if ((this.fazaID == null && other.fazaID != null) || (this.fazaID != null && !this.fazaID.equals(other.fazaID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return emertimi;
    }

}
