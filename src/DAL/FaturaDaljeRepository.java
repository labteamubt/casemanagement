/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaDalje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public class FaturaDaljeRepository extends EntMngClass implements FaturaDaljeInterface{

    public void create(FaturaDalje fd) throws CourtException {
        try {

            em.getTransaction().begin();
            em.persist(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(FaturaDalje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.merge(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(FaturaDalje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.remove(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<FaturaDalje> findAll() throws CourtException {
        try {
            return em.createNamedQuery("FaturaDalje.findAll").getResultList();
        } catch (Exception e) {
            throw new CourtException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public FaturaDalje findByID(Integer ID) throws CourtException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
