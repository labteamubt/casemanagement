/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaHyrjeDetaje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeDetajeRepository extends EntMngClass implements FaturaHyrjeDetajeInterface {

    public void create(FaturaHyrjeDetaje fd) throws CourtException {
        try {

            em.getTransaction().begin();
            em.persist(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(FaturaHyrjeDetaje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.merge(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(FaturaHyrjeDetaje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.remove(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<FaturaHyrjeDetaje> findAll() throws CourtException {
        try {
            return em.createNamedQuery("FaturaHyrjeDetaje.findAll").getResultList();
        } catch (Exception e) {
            throw new CourtException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public FaturaHyrjeDetaje findByID(Integer ID) throws CourtException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
