/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaDalje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface FaturaDaljeInterface {
    
    void create(FaturaDalje fd) throws CourtException;

    void edit(FaturaDalje fd) throws CourtException;

    void delete(FaturaDalje fd) throws CourtException;

    List<FaturaDalje> findAll() throws CourtException;

    FaturaDalje findByID(Integer ID) throws CourtException;
}
