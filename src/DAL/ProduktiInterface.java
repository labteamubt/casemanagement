/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Produkti;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface ProduktiInterface {
    
    void create(Produkti p) throws CourtException;

    void edit(Produkti p) throws CourtException;

    void delete(Produkti p) throws CourtException;

    List<Produkti> findAll() throws CourtException;

    Produkti findByID(Integer ID) throws CourtException;
}
