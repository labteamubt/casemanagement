/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.LlojiProduktit;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface LlojiProduktitInterface {

    void create(LlojiProduktit lp) throws CourtException;

    void edit(LlojiProduktit lp) throws CourtException;

    void delete(LlojiProduktit lp) throws CourtException;

    List<LlojiProduktit> findAll() throws CourtException;

    LlojiProduktit findByID(Integer ID) throws CourtException;
}
