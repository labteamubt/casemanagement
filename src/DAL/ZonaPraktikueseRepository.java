/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.ZonaPraktikuese;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public class ZonaPraktikueseRepository extends EntMngClass implements ZonaPraktikueseInterface {

    public void create(ZonaPraktikuese zp) throws CourtException {
        try {

            em.getTransaction().begin();
            em.persist(zp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(ZonaPraktikuese zp) throws CourtException {
        try {
            em.getTransaction().begin();
            em.merge(zp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(ZonaPraktikuese zp) throws CourtException {
        try {
            em.getTransaction().begin();
            em.remove(zp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<ZonaPraktikuese> findAll() throws CourtException {
        try {
            return em.createNamedQuery("ZonaPraktikuese.findAll").getResultList();
        } catch (Exception e) {
            throw new CourtException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public ZonaPraktikuese findByID(Integer ID) throws CourtException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
