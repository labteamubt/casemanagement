/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.OrganizoDetyra;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface OrganizoDetyraInterface {

    void create(OrganizoDetyra o) throws CourtException;

    void edit(OrganizoDetyra o) throws CourtException;

    void delete(OrganizoDetyra o) throws CourtException;

    List<OrganizoDetyra> findAll() throws CourtException;

    OrganizoDetyra findByID(Integer ID) throws CourtException;
}
