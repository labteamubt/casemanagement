/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaHyrje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public class FaturaHyrjeRepository extends EntMngClass implements FaturaHyrjeInterface {

    public void create(FaturaHyrje fd) throws CourtException {
        try {

            em.getTransaction().begin();
            em.persist(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(FaturaHyrje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.merge(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(FaturaHyrje fd) throws CourtException {
        try {
            em.getTransaction().begin();
            em.remove(fd);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<FaturaHyrje> findAll() throws CourtException {
        try {
            return em.createNamedQuery("FaturaHyrje.findAll").getResultList();
        } catch (Exception e) {
            throw new CourtException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public FaturaHyrje findByID(Integer ID) throws CourtException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
