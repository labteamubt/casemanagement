/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.LlojiProduktit;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public class LlojiProduktitRepository extends EntMngClass implements LlojiProduktitInterface{
    public void create(LlojiProduktit lp) throws CourtException {
        try {

            em.getTransaction().begin();
            em.persist(lp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void edit(LlojiProduktit lp) throws CourtException {
        try {
            em.getTransaction().begin();
            em.merge(lp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public void delete(LlojiProduktit lp) throws CourtException {
        try {
            em.getTransaction().begin();
            em.remove(lp);
            em.getTransaction().commit();
        } catch (Exception e) {
            throw new CourtException("Msg \n" + e.getMessage());
        }
    }

    @Override
    public List<LlojiProduktit> findAll() throws CourtException {
        try {
            return em.createNamedQuery("LlojiProduktit.findAll").getResultList();
        } catch (Exception e) {
            throw new CourtException("Msg! \n" + e.getMessage());
        }
    }

    @Override
    public LlojiProduktit findByID(Integer ID) throws CourtException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
