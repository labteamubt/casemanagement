/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaHyrje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface FaturaHyrjeInterface {

    void create(FaturaHyrje fd) throws CourtException;

    void edit(FaturaHyrje fd) throws CourtException;

    void delete(FaturaHyrje fd) throws CourtException;

    List<FaturaHyrje> findAll() throws CourtException;

    FaturaHyrje findByID(Integer ID) throws CourtException;
}
