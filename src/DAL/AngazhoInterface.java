/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Angazho;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface AngazhoInterface {

    void create(Angazho a) throws CourtException;

    void edit(Angazho a) throws CourtException;

    void delete(Angazho a) throws CourtException;

    List<Angazho> findAll() throws CourtException;

    Angazho findByID(Integer ID) throws CourtException;
}
