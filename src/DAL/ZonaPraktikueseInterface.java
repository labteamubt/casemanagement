/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.ZonaPraktikuese;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface ZonaPraktikueseInterface {

    void create(ZonaPraktikuese zp) throws CourtException;

    void edit(ZonaPraktikuese zp) throws CourtException;

    void delete(ZonaPraktikuese zp) throws CourtException;

    List<ZonaPraktikuese> findAll() throws CourtException;

    ZonaPraktikuese findByID(Integer ID) throws CourtException;
}
