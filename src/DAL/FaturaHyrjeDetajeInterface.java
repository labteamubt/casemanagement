/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.FaturaHyrjeDetaje;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface FaturaHyrjeDetajeInterface {

    void create(FaturaHyrjeDetaje f) throws CourtException;

    void edit(FaturaHyrjeDetaje f) throws CourtException;

    void delete(FaturaHyrjeDetaje f) throws CourtException;

    List<FaturaHyrjeDetaje> findAll() throws CourtException;

    FaturaHyrjeDetaje findByID(Integer ID) throws CourtException;
}
