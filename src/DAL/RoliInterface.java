/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Roli;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface RoliInterface {

    void create(Roli r) throws CourtException;

    void edit(Roli r) throws CourtException;

    void delete(Roli r) throws CourtException;

    List<Roli> findAll() throws CourtException;

    Roli findByID(Integer ID) throws CourtException;
}
