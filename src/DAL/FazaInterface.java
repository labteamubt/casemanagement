/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import BLL.Faza;
import java.util.List;

/**
 *
 * @author RinorJahaj
 */
public interface FazaInterface {

    void create(Faza f) throws CourtException;

    void edit(Faza f) throws CourtException;

    void delete(Faza f) throws CourtException;

    List<Faza> findAll() throws CourtException;

    Faza findByID(Integer ID) throws CourtException;
}
